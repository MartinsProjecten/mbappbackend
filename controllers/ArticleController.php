<?php
use Psr\Container\ContainerInterface;

require_once __DIR__ . '/../models/Article.php';
require_once __DIR__ . '/../helpers/Validate.php';
require_once __DIR__ . '/../helpers/uniqueIdGenerator.php';

class ArticleController{

  protected $Article;
  protected $db;

  public function __construct(ContainerInterface $container){
    $this->db = $container->get('db');
    $this->Article = new Article($this->db);
  }

  public function index($request, $response){
    $data = $this->Article->articles();
    return $response->withJson($data, 200);
  }

  private function validate($values) {
    $validate = new Validate($values);
    return $validate
      ->field('title')->required()->unique($this->db, 'articles')->max()
      ->field('content')->required();
  }

  public function create($request, $response){
    $userId = $request->getAttribute('userId');
    $fileId = $request->getAttribute('fileId');
    $validation = $this->validate($request->getParsedBody());
    if($validation->success()){
      $data = $validation->data();
      $data['uid'] = uniqueIdGenerator();
      $data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
      $data['accesslevel'] = 'private';
      $data['file_id'] = $fileId;
      $data['user_id'] = $userId;
    
      if($this->Article->create($data)){
      
        $jsonData = [
          'uid' => $data['uid'],
          'title' => $data['title'], 
          'content' => $data['content'],
          'fileId' => $data['file_id']
        ];
        return $response->withJson($jsonData, 201); 
      }
    }
    return $response->withJson(['errors' => $validation->errors()], 400);  
  }

  public function read($request, $response, $args){
    $code = 201;
    $data = $this->Article->read($args['uid']);
    if($data === false){
      $code = 404;
      $data = ['message' => 'Artikel niet gevonden', 'error' => true];
    }
    return $response->withJson($data, $code);
  }

  public function update($request, $response, $args){
    $values = $request->getParsedBody();
    $values['uid'] = $args['id'];
    $validate = new Validate($values);
    $validation = $validate
      ->field('title')->required()->unique($this->db, 'articles')->max()
      ->field('content')->required();
    if($validation->success()){
      $data = $validation->data();
      $data['updated_at'] = date("Y-m-d H:i:s");
      if($data['accesslevel'] === 'public') {
        $data['published_at'] = $data['updated_at'];
      }

      if($this->Article->update($data)){
        return $response->withJson($data, 201);
      }
    }
    return $response->withJson($validation->errors(), 400);
  }

  public function delete(){

  }

}