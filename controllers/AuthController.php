<?php
use Psr\Container\ContainerInterface;
use \Firebase\JWT\JWT;
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../helpers/Validate.php';
require_once __DIR__ . '/../helpers/uniqueIdGenerator.php';

class AuthController {

  public function __construct(ContainerInterface $container){
    $this->db = $container->get('db');
     $this->settings = $container->get('settings');
    $this->User = new User($this->db);
  }
/* -----------------------
REGISTER
	- de client stuurt de gegevens door
	- kijk of alle nodige gegevens er zijn: email wachtwoord en wachtwoordopnieuw
		-wanneer er gegevens ontbreken stuur een 400?
	- Wanneer er geen gegevens ontbreken valideer de gegevens
		-is de email een email, wachtwoordopnieuw zelfde als wachtwoord
			-de valitie is mislukt stuur error gegevens +422 code
		-wanneer validatie gelukt is probeer de gegevens in de database in te voeren
			- bij mislukken invoeren stuur een error er ging iets mis probeer het nog maals met code 204 of 0?
		-Bij het lukken van invoeren van gegevens, haal op een manier de gegevens van de gebruiker op voor de id
			of stuur een repsonse met 201 gelukt van het maken van de gebruiker
--------------------------- */
  public function register($request, $response){
  	$validate = new Validate($request->getParsedBody());
  	$validation = $validate
  		->field('email')->required()->email()
  		->field('password')->required()
  		->field('repeatPassword')->required()->same('password');

  	if($validation->success()){
      $data = $validation->data();
      unset($data['repeatPassword']);
      $data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
      $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
      $data['uid'] = uniqueIdGenerator();

      if($this->User->addUser($data)){
        return $response->withJson($data, 201);
      }
      return $response->withJson(['message' => 'Er ging iets mis met registeren, probeer het nogmaals!'], 204);
       
      }
      return $response->withJson($validation->errors(), 400); 

  }

/* -----------------------
LOGIN
-Bekijk de ingevoerde gegevens (email & password)
	-valideer of de email wel een email is
	-zet het wachtwoord om in een encrypted versie
	-zoek email en bijhorend wachtwoord in de database op
		-wanneer de juiste gegevens goed zijn geef de gegevens dan door id en email
		-wanneer er niet de juiste gegevens zijn gevonden(email of wachtwoord klopt niet)
			geef dan een error met auth error 400
--------------------------- */
  public function login($request, $response){
  	//validatie initializeren
  	$validate = new Validate($request->getParsedBody());
  	//validatie regels instellen en uitvoeren
  	$validation = $validate
  		->field('email')->required()->email()
  		->field('password')->required();
  	//Bij succes van de validatie email en wachtwoord pakkken
  	if($validation->success()){
  		$email = $validation->data()['email'];
  		$password = $validation->data()['password'];
      $data = $this->User->findUser($email);
  		//Uitzoeken of de gebruiker met de gegeven email gevonden kan worden
  		if($data && password_verify($password, $data['password'])){
  			//bekijken of het gegeven wachtwoord(encrypted) gelijk is aan het encrypted wachtwoord in de database
  		  return $this->setToken($request, $response, $data);
  		} 
  	}
  	else {
  		return $response->withJson(['error'=> true, 'errors' => $validation->errors()], 400); 
  	}
  	//wanneer validatie wel gelukt is maar email en of wachtwoord komt niet overeen stuur de response
  	return $response->withJson(['error'=> true, 'emailpasswordcombination'=> true, 'errors' => 'Gebruikersnaam / wachtwoord combinatie klopt niet, probeer het later nogmaals!'], 400); 
  	
  	
  }

/* -----------------------
JWT AANMAKEN
	-inloggen gelukt maak met de id van de user een nieuwe token aan.
	-probeer een try and catch blok om uit te proberen of het aanmaken van de token geluk is?
	-stuur een json respon met de token naar de client	bij het lukken van het aanmaken
	-stuur een json error 403 naar de client bij mislukken		
--------------------------- */
  private function setToken($request, $response, $data){
    $timestamp = time();
    $token = [
      "iat" => $timestamp,
      "exp" => $timestamp+(60*60*24*7), //1week geldig
      "uid" => $data['uid']
    ];
    try {
    	$jwt = JWT::encode($token, $this->settings['jwt']['secret']);
    	return $response->withJson($jwt, 201); 
    } catch (\Exception $e) {
    	return $response->withJson(['message' => 'Er ging iets mis met het inloggen, probeer het later nogmaals!'], 403);
    }
    
  }

/* -----------------------
ACCESS
	-Bij een request waar inloggen vereist is kijk of de request een header heeft met een authorization bearer field met de token
		-Wanneer er geen token is stuur een 403 naar de client
	-Kijk of de jwt token valide is door een try and catch uittevoeren op de token
		- bij invalide jwt stuur een 403
		- Bij juiste code probeer de id er uit te halen en kijk in de database of er een user bestaan met dat id.
			-Wanneer er geen gebruiker is gevonden stuur een 403
			-Wanneer de gebruiker wel bestaat return true, return the request
--------------------------- */
  public function getToken($request, $response){
    // [$jwt] = sscanf($request->getHeaderLine('Authorization'),'Bearer %s');
    $jwt = explode(" ", $request->getHeaderLine('Authorization'))[1];
  
   	if(isset($jwt)){
        try {
          $decode = JWT::decode($jwt, $this->settings['jwt']['secret'], ['HS256']);
          $id = $decode->uid;
         
          if($this->User->findUserId($id)){
          	return $response->withJson(['success' => true, 'message' => 'accessGranted'], 200); 
          }
          return $response->withJson(['message' => 'Gebruiker bestaat niet'], 403); 
        }catch (Exception $e) {
          $result = ['message' => 'Onjuiste JWT - Authenticatie mislukt!'];
          return $response->withJson($result, 403); 
        }
      }
     return $response->withJson(['401' => 'Geen toegang'], 401);  
  	
  }

  public function findID($request, $response, $args){
    //$jwt = $request->getHeaderLine('Authorization');
    return $response->withJson($this->User->findUserId($args['id']), 401);  
  }
  
}
