<?php
use Psr\Container\ContainerInterface;
//use Psr\Http\Message\StreamInterface;
require_once __DIR__ . '/../helpers/Validate.php';
require_once __DIR__ . '/../models/File.php';
require_once __DIR__ . '/../helpers/uniqueIdGenerator.php';

class FileController {

	private $File;

	public function __construct(ContainerInterface $container){
    $this->File = new File($container->get('db'));
	}


  private function moveUploadedFile($uploadedFile){
    $directory = __DIR__ . '/../uploads';
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8));
    $filename = sprintf('%s.%0.8s', $basename, $extension);
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    return $filename;
  }

	private function uploadFiles($uploadedFiles, $userId){
 
  	if($uploadedFiles->getError() === UPLOAD_ERR_OK) {
      $data['uid'] = uniqueIdGenerator();
      $data['filename'] = $this->moveUploadedFile($uploadedFiles);
  		$data['basename'] = $uploadedFiles->getClientFilename();
  		$data['size'] = $uploadedFiles->getSize();
  		$data['mimetype'] = $uploadedFiles->getClientMediaType();
  		$data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
  		$data['user_id'] = $userId;
  		if($this->File->create($data)){
        return $data;
      }
    } else {
      return $response->withJson(['error' => true], 400);
    }
	}

	public function files($request, $response){
		$uploadedFiles = $request->getUploadedFiles();
    $userId = $request->getAttribute('userId');
	
		$validate = new Validate($uploadedFiles);
		$validation = $validate
			->field('file')->isFile()->filesize('500 kb')->mimetype();
		
		if($validation->success()){
			$data = $validation->data();
			$filedata = $this->uploadFiles($data['file'], $userId);
		
			return $response->withJson($filedata);
		} 
		return $response->withJson(['error' => true, 'errors' => $validation->errors()], 400);
	}

	public function readFile1($request, $response, $args){
		$fileId = $args['fileId'];
		$fileData = $this->File->read($fileId);
		if($fileData){
			$directory = __DIR__ . '/../uploads';
			$file = $directory  . DIRECTORY_SEPARATOR . $fileData['filename'];
			if(file_exists($file)){
				$res = $response->withHeader('Content-Type', $fileData['mimetype']);
				readfile($file);
				return $res;
			}
		}
	}

	public function readFile($request, $response, $args){
		$fileId = $args['fileId'];
		$fileData = $this->File->read($fileId);
		if($fileData){
			$directory = __DIR__ . '/../uploads';
			$file = $directory  . DIRECTORY_SEPARATOR . $fileData['filename'];
			
    	$fh = fopen($file, 'rb');
    	$stream = new \Slim\Http\Stream($fh); 
    	return $response
      ->withHeader('Content-type', $fileData['mimetype'])
      ->withHeader('Content-Disposition', "inline; filename={$fileData['basename']}")
      ->withHeader('Content-Length', $fileData['size'])->withBody($stream);	
		}
	}

}
