<?php
use Psr\Container\ContainerInterface;

require_once __DIR__ . '/../models/Project.php';
require_once __DIR__ . '/../helpers/Validate.php';
require_once __DIR__ . '/../helpers/uniqueIdGenerator.php';

class ProjectController{

  protected $Project;
  protected $db;
  protected $accesslevel = 'public';

  public function __construct(ContainerInterface $container){
    $this->db = $container->get('db');
    $this->Project = new Project($this->db);
  }

  protected function validate($request){
    $validate = new Validate($request); 
    return $validate
      ->field('title')->required()->unique($this->db, 'projects')->max(255)
      ->field('content')->required();
  }

  public function index($request, $response){
    $data = $this->Project->projects($this->accesslevel);
    foreach ($data as $key => $value) {
      $data[$key] = [
        'uid' => $value['uid'],
        'title' => $value['title'],
        'content' => $value['content'],
        'created_at' => $value['created_at'],
        'file_id' => $value['file_id']
      ];
    }
    return $response->withJson($data, 200);
  }


  public function all($request, $response){
    $this->accesslevel = 'private';
    return $this->index($request, $response);
  }

  public function checktitle($request, $response) {
    $userId = $request->getAttribute('userId');
    $validate = new Validate($request->getParsedBody()); 
    $validate->field('title')->required()->unique($this->db, 'projects');
    if($validate->errors()){
      return $response->withJson(['checktitle' => true], 200);
    }
    return $response->withJson(['checktitle' => false], 200);
  }

  public function create($request, $response){
    $userId = $request->getAttribute('userId');
    $fileId = $request->getAttribute('fileId');
    var_dump($request->getParsedBody());
    die();
    $validation = $this->validate($request->getParsedBody());
    if($validation->success()){
      $data = $validation->data();
      $data['uid'] = uniqueIdGenerator();
      $data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");   
      //$data['accesslevel'] = 'private';   
      $data['file_id'] = $fileId;
      $data['user_id'] = $userId;
      if($this->Project->create($data)){
         $jsonData = [
          'uid' => $data['uid'],
          'title' => $data['title'], 
          'content' => $data['content'],
          'fileId' => $data['file_id']
        ];
        return $response->withJson($jsonData, 201); 
      }
    }
    return $response->withJson($validation->errors(), 400); 
  }

  public function read($request, $response, $args){
    $code = 200;
    $data = $this->Project->read($args['uid']);
    if($data === false){
      $code = 404;
      $data = ['message' => 'Project niet gevonden', 'error' => true];
    }
    return $response->withJson($data, $code);
  }

  public function update($request, $response, $args){
    $data = $request->getParsedBody();
    $validate = new Validate($data);
    $validation = $validate
      ->field('title')->required()->max();
    if($validation->success()){
      $data = $validation->data();
      $data['updated_at'] = date("Y-m-d H:i:s");
      $data['uid'] = $args['uid'];
      if($this->Project->update($data)){
        return $response->withJson($data, 201);
      }
      return $response->withJson(['422' => 'something went wrong'], 422);
    }
    return $response->withJson($validation->errors(), 400); 
  }

}
