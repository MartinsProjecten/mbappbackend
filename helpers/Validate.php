<?php

class Validate {

	protected $_errors = [], $_data, $field;

	public function __construct($data){
		foreach($data as $key => $value){
     $this->_data[$key] = is_object($data[$key]) ? $value : trim(filter_var($value, FILTER_SANITIZE_STRING));
		}
	}

  public function field($field){
    $this->field = $field;
    if(!isset($this->_data[$field])){
      $this->_errors[$field]['missingValues'] = 'There are 1 or more values not set, please include'; 
      $this->field = null;
    }
    
    return $this;
  }

  public function required() {
    //if(!isset($this->_data[$this->field]) || strlen($this->_data[$this->field]) === 0){
    if(!isset($this->_data[$this->field]) || strlen($this->_data[$this->field]) === 0){
      $this->_errors[$this->field]['required'] = $this->field;
    }
    return $this;
  }

  public function isFile(){
    //var_dump(is_file($this->_data[$this->field]->file));
    if(!isset($this->_data[$this->field]) || !is_file($this->_data[$this->field]->file)){
      $this->_errors[$this->field]['file'] = 'required';
    }
    return $this;
  }

  public function email(){
    $email = filter_var($this->_data[$this->field], FILTER_SANITIZE_EMAIL);
    if(!(filter_var($email, FILTER_VALIDATE_EMAIL))){
      $this->_errors[$this->field]['email'] = $this->_data[$this->field];
    }
    return $this;
  }

  public function same($compareValue){
    if(isset($compareValue) && $this->_data[$this->field] !== $this->_data[$compareValue]){
      $this->_errors[$this->field]['same'] = $compareValue;
    }
    return $this;
  }

  public function notsame($compareValue){
     if(isset($compareValue) && $this->_data[$this->field] === $this->_data[$compareValue]){
      $this->_errors[$this->field]['notSame'] = $compareValue;
    }
    return $this;
  }

  public function unique($db, $table) {
    if($this->field) {
    $sql = "SELECT uid, $this->field FROM $table WHERE $this->field=?";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(1, $this->_data[$this->field]);
    $stmt->execute();
    $result = $stmt->fetch();
    if(isset($this->_data['uid']) && $result['uid'] === $this->_data['uid']) return $this;
    if($result){
      $this->_errors[$this->field]['unique'] = $this->_data[$this->field];
    }
  }
    return $this;
  }

  public function min($min = 1){
    if(strlen($this->_data[$this->field]) < $min){
      $this->_errors[$this->field]['sizeMin'] = $min;
    }
    return $this;
  }

  public function max($max = 255){
    if($this->field) {
      if(strlen($this->_data[$this->field]) > $max){
        $this->_errors[$this->field]['sizeMax'] = $max;
      }
    }
    return $this;
  }

  public function filesize($size) {
    $bytetype = explode(" ", $size);
    $bytes =  $bytetype[0];
    $type = $bytetype[1];
    //[$bytes, $type] = sscanf($size, "%d%s");

    $times = $type === 'mb' ? (1024*1024) : $type === 'kb' ? 1024 : 1;
    if($this->_data[$this->field]->getSize() > $bytes*$times){
      $this->_errors[$this->field]['filesize'] = $size;
    }
    return $this;
  }

  public function mimetype($type=null){
    $realtype = $this->_data[$this->field]->getClientMediaType();
    $types = ['image/png', 'image/svg+xml', 'image/jpeg', 'image/gif'];
    if(isset($type)) {
      if(!in_array($type,$types) || $realtype !== $type){
        $this->_errors[$this->field]['mime-type'] = $type;
      }
    }
    elseif($realtype !== $types[array_search($realtype,$types)]){
      $this->_errors[$this->field]['mime-type'] = $realtype . ' is niet toegestaan, probeer een ander bestand!';
    }
    return $this;
  }

  public function data(){
    return $this->_data;
  }

  public function success(){
    return empty($this->_errors)?:false;
  }

  public function errors(){
    return empty($this->_errors) ? false : $this->_errors;
  }

}
