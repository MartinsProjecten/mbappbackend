<?php
// Application middleware
// use Psr\Http\Message\ServerRequestInterface as Request;
  //use Psr\Http\Message\ResponseInterface as Response;
  //use Slim\Http\Request as Request;
// e.g: $app->add(new \Slim\Csrf\Guard);

//  $app->options('/{routes:.+}', function ($request, $response, $args) {
//     return $response;
// });

$uriChecker = function($request, $response, $next){
	$uri = explode('/', $request->getUri()->getPath());
	$reactRoutes = ['artikelen', 'projecten', 'info', 'login'];
	if($uri[0] !== 'api'){
		if(in_array($uri[0], $reactRoutes)){
			return $this->renderer->render($response, 'index.phtml');
		}
	}
  $response = $next($request, $response);

	return $response;
};

$app->add($uriChecker);
