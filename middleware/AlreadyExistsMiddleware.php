<?php
use Psr\Container\ContainerInterface;
class AlreadyExistsMiddleware {

	public $db;

	public function __construct(ContainerInterface $container){
	    $this->db = $container->get('db');
	 }
	
	public function title($request, $response, $next) {

		$body = $request->getParsedBody();
		$title = trim(strtolower($body['title']));
		$uri = explode('/', $request->getUri()->getPath());
		$tableName = $uri[1];

		//find out of the requested title already exist in the database
		$stmt = $this->db->prepare("SELECT id, title FROM articles WHERE title=?");
		$stmt->execute([$title]);
		$result = $stmt->fetch();

		//if the update title it the same title as the orginal title then just continue
		$route = $request->getAttribute('route');
    	$articleId = $route->getArgument('id');
		if($articleId !== null){
			if($result['id'] === $articleId && strtolower($result['title']) === $title){
				return $next($request, $response);
			}
		}

		//if there are no title matches then continue
		if(!$result) return $next($request, $response);

		//return to frontend wiht message and error
		return $response->withJson([
			'message' => 'Titel bestaat al, kies een andere titel.', 
			'error' => true,
			'alreadyExists' => true
		], 400);

	}

}
