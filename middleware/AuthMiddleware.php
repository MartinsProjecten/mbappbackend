<?php
use Psr\Container\ContainerInterface;
use \Firebase\JWT\JWT;
require_once __DIR__ . '/../models/User.php';

class AuthMiddleware {
  public $User;
  public $settings;

  public function __construct(ContainerInterface $container){
    $this->settings = $container->get('settings');
    $this->User = new User($container->get('db'));
  }

  public function getToken($request, $response, $next){
    $jwt = explode(" ", $request->getHeaderLine('Authorization'));
    if(isset($jwt[1])){
      $jwt = $jwt[1];
        try {
          $decode = JWT::decode($jwt, $this->settings['jwt']['secret'], ['HS256']);
          $id = $decode->uid;
          if($this->User->findUserId($id)){
            $request = $request->withAttribute('userId', $id);
            return $next($request, $response);
          }
          return $response->withJson(['message' => 'Gebruiker bestaat niet'], 400); 
        }catch (Exception $e) {
          $result = ['message' => 'Onjuiste JWT - Authenticatie mislukt!'];
          return $response->withJson($result, 403); 
        }
      }
      return $response->withJson(['message' => 'Login om gebruik te maken van deze pagina'], 403);
  }
}
