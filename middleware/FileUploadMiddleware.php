<?php
use Psr\Container\ContainerInterface;
require_once __DIR__ . '/../models/File.php';
require_once __DIR__ . '/../helpers/Validate.php';
require_once __DIR__ . '/../helpers/uniqueIdGenerator.php';

class FileUploadMiddleware {
  public $File;

  public function __construct(ContainerInterface $container){
    $this->File = new File($container->get('db'));
  }

  private function moveUploadedFile($uploadedFile){
    $directory = __DIR__ . '/../uploads';
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8));
    $filename = sprintf('%s.%0.8s', $basename, $extension);
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    return $filename;
  }

  private function removeFile($file_id) {
    $directory = __DIR__ . '/../uploads';
    $file = $this->File->read($file_id);
    $path = $directory . DIRECTORY_SEPARATOR . $file['filename'];
    if(file_exists($path)){
      return unlink($path);
    }
    return false;
  }

  private function uploadFiles($uploadedFiles, $userId){
    if($uploadedFiles->getError() === UPLOAD_ERR_OK) {
      $data['uid'] = uniqueIdGenerator();
      $data['filename'] = $this->moveUploadedFile($uploadedFiles);
      $data['basename'] = $uploadedFiles->getClientFilename();
      $data['size'] = $uploadedFiles->getSize();
      $data['mimetype'] = $uploadedFiles->getClientMediaType();
      $data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
      $data['user_id'] = $userId;
      if($this->File->create($data)){
        return $data;
      }
    } else {
      return $response->withJson(['error' => true], 400);
    }
  }

  public function fileUpload($request, $response, $next){
    
    $file = $request->getUploadedFiles();
    $userId = $request->getAttribute('userId');
    if($request->isPut()){
      $data = $request->getParsedBody();
      if(count($file) === 0){
        $request = $request->withAttribute('userId', $userId);
        return $next($request, $response);
      }
      $removed = removeFile($data['file_id']);
      if($removed === false){
        return $response->withJson(['error' => true], 400);
      }
      
    }

    $validate = new Validate($file);
    $validation = $validate->field('file')
      ->isFile()->filesize('1000 kb')->mimetype();
    if($validation->success()){
      $file = $validation->data();
      $filedata = $this->uploadFiles($file['file'], $userId);
      $request = $request->withAttribute('fileId', $filedata['uid']);
      $request = $request->withAttribute('userId', $userId);
      return $next($request, $response);
    } 
    return $response->withJson(['error' => true], 400);
    
  }
}
