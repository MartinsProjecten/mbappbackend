<?php
require_once __DIR__ . '/CoreModel.php';

class Article extends CoreModel{

	public function __construct($db){
		$this->db = $db;
		$this->table = 'articles';
	}
	
	public function articles(){
	   return $this->all();
	}
}
