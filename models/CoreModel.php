<?php
class CoreModel {
	protected $db, $table = 'tableName';

	public function __construct($db){
		$this->db = $db;
	}

	public function all($accesslevel = 'private'){
	    $stmt = $this->db->prepare("SELECT * FROM $this->table WHERE accesslevel=? ORDER BY id ASC");
	    $stmt->execute([$accesslevel]);
	    $arr = $stmt->fetchAll();
	    if(!$arr) return [];
	    return $arr;
	}

	public function read($uid) {
		$stmt = $this->db->prepare("SELECT * FROM $this->table WHERE uid=?");
		$stmt->execute([$uid]);
		$item = $stmt->fetch();
		if(!$item) return false;
	    return $item;
	}

	public function update($data){
	 	$sqlValues = $this->patch($data);      
		$sql = "UPDATE $this->table SET $sqlValues WHERE uid=:uid";
		$stmt= $this->db->prepare($sql);
		$stmt->execute($data);
		return $stmt->rowCount();
	}

	protected function patch($data){
	    foreach($data as $key => $value){
	       if($key == 'uid' || $key == 'user_id' || $key == 'created_at') continue;
	       $sqlValues[] = "{$key}=:{$key}";
	   	}
	   	return implode(", ", $sqlValues);
	}

	public function create($data){
		$sqlValues = $this->make($data);
		$sql = "INSERT INTO $this->table ({$sqlValues['keys']}) 
			VALUES ({$sqlValues['values']})";
		$stmt= $this->db->prepare($sql);
		return $stmt->execute($data);
		//return (int)$this->db->lastInsertId();
	}

	protected function make($data){
	    foreach($data as $key => $value){
	       $sqlValues['keys'][] = "{$key}";
	       $sqlValues['values'][] = ":{$key}";
	   	}
	   	$sqlValues['keys'] = implode(", ",$sqlValues['keys']);
	   	$sqlValues['values'] = implode(", ",$sqlValues['values']);
	   	return $sqlValues;
	}
}
