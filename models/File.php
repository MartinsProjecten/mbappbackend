<?php

require_once __DIR__ . '/CoreModel.php';

class File extends CoreModel{

	public function __construct($db){
		$this->db = $db;
		$this->table = 'files';
	}
}
