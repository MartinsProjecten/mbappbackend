<?php

require_once __DIR__ . '/CoreModel.php';

class Project extends CoreModel{

	public function __construct($db){
		$this->db = $db;
		$this->table = 'projects';
	}
	
	public function projects($level = 'public'){
	   return $this->all($level);
	}

}
