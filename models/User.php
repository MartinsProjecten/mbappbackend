<?php
class User {

	public function __construct($db){
		$this->db = $db;
	}

	public function addUser($data){
		$sql = "INSERT INTO users (uid, email, password, created_at, updated_at) VALUES (:uid, :email, :password, :created_at, :updated_at)";
		$prepare = $this->db->prepare($sql);
		return $prepare->execute($data);
	}

	public function findUser($email){
		$sql = "SELECT uid, email, password FROM users WHERE email = ?";
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$email]);
		$user = $stmt->fetch();
		return $user ?: false;
		
	}

	public function findUserId($uid){
		$sql = "SELECT uid FROM users WHERE uid = ?";
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$uid]);
		return $stmt->fetch();
	}

}
