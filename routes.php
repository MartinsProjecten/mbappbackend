<?php

// use Slim\Http\Request;
// use Slim\Http\Response;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
// Routes

$routes = __DIR__ . '/routes/';
require_once $routes . 'web.php';
require_once $routes . 'api.php';


$app->get('/', function (Request $request, Response $response) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    return $this->renderer->render($response, 'index.phtml');
});


$app->get('/paper', function (Request $request, Response $response) {
  $data = array('name' => $request->getMethod(), 'age' => $response->getStatusCode());
  return $response->withJson($data);

});
