<?php
require_once __DIR__ . '/../controllers/ArticleController.php';
require_once __DIR__ . '/../controllers/ProjectController.php';
require_once __DIR__ . '/../controllers/AuthController.php';
require_once __DIR__ . '/../controllers/FileController.php';
require_once __DIR__ . '/../middleware/AuthMiddleware.php';
require_once __DIR__ . '/../middleware/AlreadyExistsMiddleware.php';
require_once __DIR__ . '/../middleware/FileUploadMiddleware.php';


$app->group('/api', function($app) {

  //Api Article Routes
  $app->group('/articles', function($app) {
    $this->get('', \ArticleController::class.':index'); //->add('AuthMiddleware:getToken');
    $this->post('', '\ArticleController:create');
    $this->get('/{uid}', '\ArticleController:read');
    $this->put('/{id}', '\ArticleController:update'); 
    $this->delete('/{id}', function(){});
  });

  //Api User routes
  $app->group('/users', function($app) {
    $this->post('', '\AuthController:register');
    $this->post('/auth', '\AuthController:login');
    $this->get('/auth', '\AuthController:getToken');
    $this->get('/{id}', '\AuthController:findID');
  });

  //Api Project routes
  $app->group('/projects', function($app){
    $this->get('', '\ProjectController:index');
    $this->get('/all', '\ProjectController:all')->add('AuthMiddleware:getToken');
    $this->post('', '\ProjectController:create')
      ->add('FileUploadMiddleware:fileUpload')
      ->add('AuthMiddleware:getToken');

    $this->get('/{uid}', '\ProjectController:read');
    $this->post('/checktitle', '\ProjectController:checktitle'); //->add('AuthMiddleware:getToken');
    $this->patch('/{uid}', '\ProjectController:update')
      ->add('FileUploadMiddleware:fileUpload')
      ->add('AuthMiddleware:getToken');
  });

  $app->post('/files', '\FileController:files')->add('AuthMiddleware:getToken');
  $app->get('/files/{fileId}', '\FileController:readFile');
});
