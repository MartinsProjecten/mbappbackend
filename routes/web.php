<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/../controllers/AuthController.php';
use Psr\Container\ContainerInterface;

$app->get('/weer', function(Request $request, Response $response){
    return $this->renderer->render($response, 'projects/hetweer.php');
});

$app->get('/js/test.js', function(Request $request, Response $response, $args){
  readfile(__DIR__ . '/../../templates/js/test.js');
  return $response->withHeader(
        'Content-Type',
        'application/javascript'
    );
});
